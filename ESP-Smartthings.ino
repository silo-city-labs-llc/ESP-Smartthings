#include <FS.h>
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include "libraries/aREST.h"
#include "libraries/WiFiManager.h"

aREST rest = aREST();
WiFiServer server(80);
int lastState = 0;
int switchPin = 12;//GPIO12
int lightPin = 13;//GPIO13

void setup() {
  Serial.begin(115200);
  Serial.println();

  //WiFiManager
  WiFiManager wifiManager;
  wifiManager.setBreakAfterConfig(true); //exit after config instead of connecting

  wifiManager.resetSettings();//reset settings - for testing

  //tries to connect to last known settings
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect, we should reset as see if it connects");
    delay(3000);
    ESP.reset();
    delay(5000);
  }
  
  rest.set_id("132654");
  rest.set_name("esp8266");

  pinMode(lightPin, OUTPUT);
  lastState = digitalRead(switchPin);
  
  server.begin();
}

void loop() {
  if(lastState  != digitalRead(switchPin)){
    lastState = digitalRead(switchPin);
    digitalWrite(lightPin, digitalRead(lightPin)? 0:1);
    delay(1000);
  }
  
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while(!client.available()){
    delay(1);
  }
  rest.handle(client);
}
