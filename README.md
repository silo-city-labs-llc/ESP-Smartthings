# ESP-Smartthings
ESP8266 Smart Things Hub devices

## Supported Devices
These devices will be the only ones I will support at this moment, They have many gpio pins, and high memory and storage.

* [ESP-07](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F201314957928)
* [ESP-08](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F400985274684)
* [ESP-12](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F121951859776)
* [ESP-12E](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F121951859776)
* [ESP-12F](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F272408386985)
* [ESP-13](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F182388114979)
* [ESP-14](https://rover.ebay.com/rover/1/711-53200-19255-0/1?icep_id=114&ipn=icep&toolid=20004&campid=5338203480&mpre=https%3A%2F%2Fwww.ebay.com%2Fitm%2F182388114979)

## Features
* Toggle Physical Switch
* Relay for remote switching
  * 3.3v relays
  * Latched relays?
  * Solid state relays?
* SmartThings Hub Control

## Planned features
* 433mhz hub addition for cheaper sensors, also being able to tag along a existing powered esp to save resources.

## Wiring schematics

### Light Switch
Pin: GPIO12
Images coming soon

### Relay
Pin: GPIO13
Images coming soon, in the meantime there is a esp-01 image in the repository.

## How to compile

You can compile this by opening the main `ESP-Smartthings.ino` file in [Arduino IDE](https://www.arduino.cc/en/Main/Software), then `Export compiled Binary` or `Upload`

## How to install

Wire up your esp-12 for flash mode, unless you have a custom board, the flip your flash mode switch.

![esp12 flash mode](/images/ESP8266-esp-12-flash-mode.png?raw=true "esp-12 flash mode")

Using the releases tab, you may flash the .bin file using one of the following programs:

* [esptool-gui](https://github.com/Rodmg/esptool-gui/releases)
* [esp8266-reflash](http://www.xess.com/blog/esp8266-reflash/)
* [others](http://www.instructables.com/id/Intro-Esp-8266-firmware-update/)

## Credits
* aRest - https://github.com/marcoschwartz/aREST (MIT)
* WifiManager - https://github.com/tzapu/WiFiManager (MIT)
